@default_files = (
    'main.tex',
);
$jobname = $ENV{'JOBNAME'} || 'main';
$out_dir = 'build';
$pdf_mode = 5;
$go_mode = 1;
