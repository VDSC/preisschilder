#!/usr/bin/env python3
url = "https://tex.zih.tu-dresden.de/read/tnqvpxpfszxp"
branch = "sharelatex"

import json
import re
import subprocess
import sys
import time
import zipfile
from functools import partial
from io import BytesIO
from tempfile import TemporaryDirectory
from urllib.error import HTTPError
from urllib.parse import urljoin
from urllib.request import Request, urlopen


def eprint(*args, file=sys.stderr, **kwargs):
    print(*args, file=file, **kwargs)


def run(*cmd, check=True, **kwargs):
    eprint("$", *cmd)
    return subprocess.run(cmd, check=check, **kwargs)


def fetch(url, data=None, headers={}):
    while True:
        eprint("urlopen(Request(%r, %r, %r))" % (url, data, headers))
        try:
            return urlopen(Request(url, data, headers))
        except HTTPError as e:
            if e.code == 429:
                eprint("Too Many Requests, waiting 60 seconds...")
                time.sleep(60)
            else:
                raise


def has_branch(branch):
    try:
        run("git", "rev-parse", "--verify", branch)
    except subprocess.CalledProcessError:
        return False
    else:
        return True


def get_sharelatex_url_and_cookie(url):
    resp = fetch(url)
    text = resp.read().decode("utf-8")

    cookie = resp.getheader("Set-Cookie")
    assert cookie, "cannot get ShareLaTeX session cookie"
    cookie = cookie.split(";", 1)[0]

    postUrl = re.search(r'"postUrl"\s*:\s*("(?:[^"]|\\.)*")', text)
    assert postUrl, "cannot find ShareLaTeX postUrl"
    postUrl = json.loads(postUrl[1])
    postUrl = urljoin(resp.url, postUrl)

    csrfToken = re.search(r'"csrfToken"\s*:\s*("(?:[^"]|\\.)*")', text)
    assert csrfToken, "cannot find ShareLaTeX csrfToken"
    csrfToken = json.loads(csrfToken[1])

    resp = fetch(
        postUrl,
        json.dumps({"_csrf": csrfToken}).encode("utf-8"),
        {"Content-Type": "application/json", "Cookie": cookie},
    )
    data = json.loads(resp.read().decode("utf-8"))

    return urljoin(resp.url, data["redirect"]), cookie


def get_sharelatex_updates(url, cookie):
    url += "/updates"
    resp = fetch(url, None, {"Cookie": cookie})
    x = json.loads(resp.read().decode("utf-8"))
    assert x.keys() == {"updates"}
    return x["updates"]


def get_sharelatex_zip_url(url, version=None):
    if version is None:
        return url + "/download/zip"
    else:
        return "%s/version/%d/zip" % (url, version)


if __name__ == "__main__":
    new_branch = not has_branch(branch)
    with TemporaryDirectory() as tmp:
        if new_branch:
            run("git", "worktree", "add", "-d", tmp)
        else:
            run("git", "worktree", "add", tmp, branch)
        try:
            run_git = partial(run, "git", "-C", tmp)
            if new_branch:
                run_git("checkout", "--orphan", branch)
            run_git("rm", "-fr", ".")

            eprint("Accessing ShareLaTeX project...")
            url, cookie = get_sharelatex_url_and_cookie(url)
            try:
                updates = get_sharelatex_updates(url, cookie)
            except HTTPError as e:
                zipurls = [(None, None, get_sharelatex_zip_url(url))]
            else:
                latest = run_git(
                    "log",
                    "-n",
                    "1",
                    "--format=%at",
                    check=False,
                    stdout=subprocess.PIPE,
                )
                if not latest.stdout:
                    latest = 0
                else:
                    latest = int(latest.stdout.decode("utf-8"), 10)
                zipurls = [
                    (
                        u["toV"],
                        u["meta"]["end_ts"] / 1000,
                        get_sharelatex_zip_url(url, u["toV"]),
                    )
                    for u in reversed(updates)
                    if u["meta"]["end_ts"] / 1000 > latest
                ]

            do_merge = False
            for version, t, zipurl in zipurls:
                t = t and time.strftime("%Y-%m-%d %H:%M:%S+00:00", time.gmtime(t))
                eprint(
                    "Downloading ShareLaTeX zip%s..." % ("" if t is None else " " + t)
                )
                zip_data = fetch(zipurl, None, {"Cookie": cookie}).read()

                eprint("Extracting zip...")
                with zipfile.ZipFile(BytesIO(zip_data)) as zip:
                    zip.extractall(tmp)
                run_git("add", "-v", ".")
                p = run_git(
                    "diff", "--no-patch", "--exit-code", "--staged", check=False
                )

                if p.returncode == 0:
                    run_git("status")
                else:
                    args = ["commit"]
                    if version is None:
                        args.extend(["-m", "download from ShareLaTeX"])
                    else:
                        args.extend(
                            ["--date=" + t, "-m", "download version %d" % version]
                        )
                    run_git(*args)
                    do_merge = True

            if do_merge:
                run("git", "merge", "--allow-unrelated-histories", branch)
        finally:
            run("git", "worktree", "remove", "-f", tmp)
